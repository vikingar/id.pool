package space.chemodurov.id.pool.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import space.chemodurov.id.pool.entity.Pool;
import space.chemodurov.id.pool.service.PoolService;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

@Controller
@RequiredArgsConstructor
@Slf4j
public class PoolController {

    private final PoolService poolService;

    @GetMapping("/next-pool")
    public CompletableFuture<ResponseEntity<Pool>> getId() {
        return poolService.nextPool()
                .thenApply(ResponseEntity::ok)
                .exceptionally(handleException);
    }

    private static final Function<Throwable, ResponseEntity<Pool>> handleException =
        ex -> {
            log.error("OOOPS!", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        };
}
