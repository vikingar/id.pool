package space.chemodurov.id.pool.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Pool {
    private Long min;
    private Long max;
}
