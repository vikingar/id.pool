FROM openjdk:11-jre-slim-stretch
LABEL maintainer="Chemodurov Vadim vv.chemodurov@gmail.com"
ARG APP_HOME=/app
WORKDIR $APP_HOME
COPY /target/*.jar $APP_HOME/pool.jar
ENTRYPOINT java $JAVA_OPTS -jar pool.jar $JAVA_ARGS