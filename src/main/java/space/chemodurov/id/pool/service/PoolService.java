package space.chemodurov.id.pool.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import space.chemodurov.id.pool.entity.Pool;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class PoolService {

    @Value("${pool.size}")
    private Long poolSize;

    private final AtomicReference<Long> pool = new AtomicReference<>(0L);

    @Async
    public CompletableFuture<Pool> nextPool() {
        Long min = pool.updateAndGet(pool -> pool + poolSize);
        return CompletableFuture.completedFuture(
                new Pool()
                .setMin(min)
                .setMax(min + poolSize)
        );
    }
}
